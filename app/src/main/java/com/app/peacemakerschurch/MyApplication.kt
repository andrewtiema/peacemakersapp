package com.app.peacemakerschurch

import android.app.Application
import com.app.peacemakerschurch.di.AppComponent
import com.app.peacemakerschurch.di.DaggerAppComponent

open class MyApplication : Application() {

    val appComponent : AppComponent by lazy {
        DaggerAppComponent.factory().create(this)
    }
}