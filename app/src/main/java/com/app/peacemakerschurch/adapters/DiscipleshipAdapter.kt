package com.app.peacemakerschurch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.peacemakerschurch.databinding.DiscipleshipItemBinding
import com.app.peacemakerschurch.db.entities.Fellowship

class DiscipleshipAdapter(private val clickListener: OnClickListener) : ListAdapter<Fellowship,DiscipleshipAdapter.DiscipleshipAdapterViewHolder>(DiscipleshipDiffUtil) {

    inner class DiscipleshipAdapterViewHolder(private val binding : DiscipleshipItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item : Fellowship) {
            binding.title.text = item.title
            binding.description.text = item.description
            binding.date.text = item.date
        }


    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DiscipleshipAdapterViewHolder {
        return DiscipleshipAdapterViewHolder(DiscipleshipItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: DiscipleshipAdapterViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)

        holder.itemView.setOnClickListener {
            clickListener.onClick(item)
        }
    }

    companion object DiscipleshipDiffUtil : DiffUtil.ItemCallback<Fellowship>() {
        override fun areItemsTheSame(oldItem: Fellowship, newItem: Fellowship): Boolean {
            return oldItem === oldItem
        }

        override fun areContentsTheSame(oldItem: Fellowship, newItem: Fellowship): Boolean {
            return oldItem.id == newItem.id
        }

    }

    class OnClickListener(val clickListener : (discipleship : Fellowship)->Unit) {
        fun onClick(discipleship : Fellowship) = clickListener(discipleship)
    }
}