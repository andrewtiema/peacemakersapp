package com.app.peacemakerschurch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.peacemakerschurch.R
import com.app.peacemakerschurch.databinding.NewsItemBinding
import com.app.peacemakerschurch.db.entities.News
import com.app.peacemakerschurch.util.SERMON_URL
import com.squareup.picasso.Picasso

class NewsAdapter(private val clickListener:OnClickListener) : `ListAdapter`<News,NewsAdapter.NewsViewHolder>(NewsDiffUtil) {

    inner class NewsViewHolder(private val binding : NewsItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: News) {
            binding.title.text = item.title
            binding.description.text = item.description
            binding.date.text = item.date
        }
        val image = binding.imageView2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsAdapter.NewsViewHolder {
        return NewsViewHolder(NewsItemBinding.inflate(LayoutInflater.from(parent.context), parent,false ))
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
        Picasso.get().load(SERMON_URL + item.featured_photo).placeholder(R.drawable.white_background).error(
            R.drawable.white_background).into(holder.image)
        holder.itemView.setOnClickListener {
            clickListener.onClick(item)
        }

    }


    companion object NewsDiffUtil : DiffUtil.ItemCallback<News>() {
        override fun areItemsTheSame(oldItem: News, newItem: News): Boolean {
            return oldItem == oldItem
        }

        override fun areContentsTheSame(oldItem: News, newItem: News): Boolean {
            return oldItem.id == newItem.id
        }

    }

    class OnClickListener(val clickListener : (news : News)->Unit) {
        fun onClick(news : News) = clickListener(news)
    }

}