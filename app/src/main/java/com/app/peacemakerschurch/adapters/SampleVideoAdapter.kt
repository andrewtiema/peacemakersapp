package com.app.peacemakerschurch.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.peacemakerschurch.databinding.SingleVideoItemBinding
import com.app.peacemakerschurch.db.entities.Sermon
import com.app.peacemakerschurch.util.SERMON_URL
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard


class SampleVideoAdapter(private val clickListener: OnClickLickListener) :
    ListAdapter<Sermon, SampleVideoAdapter.MyViewHolder>(SampleVideoDiffUtil) {

    inner class MyViewHolder(private val binding: SingleVideoItemBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {

        val tv: TextView = binding.tv
        val description = binding.description
        val videoPlayer = binding.videoPalyer
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SampleVideoAdapter.MyViewHolder {
        return MyViewHolder(SingleVideoItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val item = getItem(position)
        holder.tv.text = item.title
        holder.description.text = item.description
        //Set clickListener for the sermon object
        holder.itemView.setOnClickListener {
            clickListener.onClick(item)
        }

        getVideo(SERMON_URL + item.url, holder.videoPlayer)
    }


    private fun getVideo(url: String?, video: JCVideoPlayerStandard) {
        video.setUp(url, JCVideoPlayerStandard.SCREEN_LAYOUT_LIST)
        video.battery_level.visibility = View.GONE
        video.batteryTimeLayout.visibility = View.GONE
        video.backButton.visibility = View.GONE

        //auto play
      //  video.startButton.performClick()

    }

    companion object SampleVideoDiffUtil : DiffUtil.ItemCallback<Sermon>() {
        override fun areItemsTheSame(oldItem: Sermon, newItem: Sermon): Boolean {
            return oldItem == oldItem
        }

        override fun areContentsTheSame(oldItem: Sermon, newItem: Sermon): Boolean {
            return oldItem.id == newItem.id
        }

    }


    class OnClickLickListener(val clickListener: (sermon: Sermon) -> Unit) {
        fun onClick(sermon: Sermon) = clickListener(sermon)
    }

}