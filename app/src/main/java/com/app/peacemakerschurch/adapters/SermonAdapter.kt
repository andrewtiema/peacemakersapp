package com.app.peacemakerschurch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.peacemakerschurch.R
import com.app.peacemakerschurch.databinding.SermonItemMoreBinding
import com.app.peacemakerschurch.db.entities.Sermon
import com.app.peacemakerschurch.util.SERMON_URL
import com.squareup.picasso.Picasso

class SermonAdapter(private val clickListener:OnClickListener) : ListAdapter<Sermon, SermonAdapter.SermonViewHolder>(SermonDiffUtil){

    inner class SermonViewHolder(private val binding: SermonItemMoreBinding) : RecyclerView.ViewHolder(binding.root) {

        val title = binding.title
        val description = binding.description
        val image = binding.imageView2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SermonAdapter.SermonViewHolder {
        return SermonViewHolder(SermonItemMoreBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: SermonAdapter.SermonViewHolder, position: Int) {
        val item = getItem(position)
        holder.title.text = item.title
        holder.description.text = item.description
        Picasso.get().load(SERMON_URL + item.featured_photo).placeholder(R.drawable.white_background).error(R.drawable.white_background).into(holder.image)
        //set click listener
        holder.itemView.setOnClickListener {
            clickListener.onClick(item)
        }
    }

    companion object SermonDiffUtil : DiffUtil.ItemCallback<Sermon>() {
        override fun areItemsTheSame(oldItem: Sermon, newItem: Sermon): Boolean {
            return oldItem == oldItem
        }

        override fun areContentsTheSame(oldItem: Sermon, newItem: Sermon): Boolean {
            return oldItem.id ==  newItem.id
        }

    }

    class OnClickListener(val clickListener: (sermon:Sermon)->Unit) {
        fun onClick(sermon:Sermon) = clickListener(sermon)
    }

}