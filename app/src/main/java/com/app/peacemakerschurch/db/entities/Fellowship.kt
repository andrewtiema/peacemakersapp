package com.app.peacemakerschurch.db.entities

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Fellowship(
    @Json(name = "id") val id: Int?,
    @Json(name = "title") val title: String?,
    @Json(name = "description") val description: String?,
    @Json(name = "url") val url: String?,
    @Json(name = "date") val date: String?,
    @Json(name = "day_id") val day_id: Int?,
    @Json(name = "created_at") val created_at: String?,
    @Json(name = "updated_at") val updated_at: String?,
    @Json(name = "day") val day: Day
): Parcelable