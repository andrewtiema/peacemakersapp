package com.app.peacemakerschurch.db.entities

import com.squareup.moshi.Json

data class FellowshipResponse (
    @Json(name = "response_status") val response_status : Int?,
    @Json(name = "response_message") val response_message : String?,
    @Json(name = "data") val data : List<Fellowship>
)