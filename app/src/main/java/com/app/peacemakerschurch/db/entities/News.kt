package com.app.peacemakerschurch.db.entities

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize


@Parcelize
data class News (
    @Json(name = "id") val id : Int,
    @Json(name = "title") val title : String,
    @Json(name = "description") val description :  String,
    @Json(name = "featured_photo") val featured_photo : String,
    @Json(name = "date") val date : String
):Parcelable