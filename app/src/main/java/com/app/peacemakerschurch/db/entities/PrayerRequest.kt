package com.app.peacemakerschurch.db.entities

import androidx.room.Ignore
import com.squareup.moshi.Json


data class PrayerRequest (
    @Ignore
    @Json(name = "id") val id : Int,
    @Json(name = "name") val name : String,
    @Json(name = "email") val email : String,
    @Json(name = "phone") val phone: String,
    @Json(name = "description") val description : String,
    @Json(name = "updated_at") val updated_at : String,
    @Json(name = "created_at") val created_at : String
)