package com.app.peacemakerschurch.di

import android.content.Context
import com.app.peacemakerschurch.di.NetworkModule.NetworkModule
import com.app.peacemakerschurch.fragments.*
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class])
interface AppComponent {

    @Component.Factory
    //Providing Context to the graph
    interface Factory {
        fun create(@BindsInstance context: Context) :  AppComponent
    }

    fun inject(activity : SermonFragment)
    fun inject(activity : SermonDetailFragment)
    fun inject(activity : NewsFragment)
    fun inject(activity : NewsDetailFragment)
    fun inject(activity : PrayerRequestFragment)
    fun inject(activity : DiscipleshipFragment)
    fun inject(activity : DiscipleshipAudioFragment)

}