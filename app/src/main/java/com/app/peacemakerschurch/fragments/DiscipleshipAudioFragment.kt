package com.app.peacemakerschurch.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.app.peacemakerschurch.MyApplication
import com.app.peacemakerschurch.R
import com.app.peacemakerschurch.databinding.FragmentDiscipleshipAudioBinding
import com.app.peacemakerschurch.util.FELLOWSHIP_URL
import com.app.peacemakerschurch.viewModel.DiscipleshipAudioViewModel
import com.example.jean.jcplayer.JcAudio
import com.example.jean.jcplayer.JcPlayerView
import java.util.ArrayList

class DiscipleshipAudioFragment : Fragment() {

    lateinit var discipleshipAudioViewModel: DiscipleshipAudioViewModel

   private lateinit var jcplayerview : JcPlayerView


    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MyApplication).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding : FragmentDiscipleshipAudioBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_discipleship_audio,container,false)
        binding.lifecycleOwner = this

        //selected Discipleship
        val selectedDiscipleship = DiscipleshipAudioFragmentArgs.fromBundle(requireArguments()).selectedDiscipleship
        discipleshipAudioViewModel = DiscipleshipAudioViewModel(selectedDiscipleship)

        //setup binding viewmodel
        binding.viewModel = discipleshipAudioViewModel


        //setup audio play
         jcplayerview = binding.jcplayerView
        val jcAudios = ArrayList<JcAudio>()

        jcplayerview.createNotification()
        jcplayerview.initPlaylist(jcAudios)

        jcAudios.add(JcAudio.createFromURL("url audio", FELLOWSHIP_URL+selectedDiscipleship.url))



        return binding.root
    }

    override fun onPause() {
        super.onPause()
        jcplayerview.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        jcplayerview.kill()
    }
}