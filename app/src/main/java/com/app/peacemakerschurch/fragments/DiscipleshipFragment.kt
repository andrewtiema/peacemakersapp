package com.app.peacemakerschurch.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.app.peacemakerschurch.MyApplication
import com.app.peacemakerschurch.R
import com.app.peacemakerschurch.adapters.DiscipleshipAdapter
import com.app.peacemakerschurch.databinding.FragmentDiscipleshipBinding
import com.app.peacemakerschurch.db.entities.Fellowship
import com.app.peacemakerschurch.db.entities.News
import com.app.peacemakerschurch.util.*
import com.app.peacemakerschurch.viewModel.DiscipleshipViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class DiscipleshipFragment : Fragment() {

    @Inject
    lateinit var viewModel : DiscipleshipViewModel

    private lateinit var adapter: DiscipleshipAdapter
    private lateinit var binding: FragmentDiscipleshipBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MyApplication).appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
         binding  = DataBindingUtil.inflate(inflater,
            R.layout.fragment_discipleship,container,false)


        //observe data
        observeData()

        //set up adapter
        setUpAdapter()

        //navigate to selected discipleship
        navigateToSelectedItem()

        return binding.root
    }
    private fun observeData() {
        viewModel.response.observe(viewLifecycleOwner, Observer {
            it?.let {response ->
                when(response) {
                    is Success<*> -> displayData(response.data as List<Fellowship>)
                    is Error -> {
                        errorDisplay()
                        handleApiError(response) { retry() }
                    }
                }
            }
        })
    }

    private fun setUpAdapter() {
         adapter = DiscipleshipAdapter(DiscipleshipAdapter.OnClickListener{
            viewModel.navigateToSelectedDiscipleship(it)
        })
        binding.recyclerview.adapter = adapter
    }

    private fun navigateToSelectedItem() {
        viewModel.navigateToSelectedDiscipleship.observe(viewLifecycleOwner, Observer {
            if (it != null){
                this.findNavController().navigate(DiscipleshipFragmentDirections.actionDiscipleshipFragmentToDiscipleshipAudioFragment(it))
                viewModel.navigateToSelectedDiscipleshipDone()
            }
        })
    }

    private fun displayData(data : List<Fellowship>) {
        hideLoadingAnimation()
        adapter.submitList(data)
    }

    private fun hideLoadingAnimation() {
        binding.loadingImageStatus.hide()
    }

    private fun errorDisplay() {
        hideLoadingAnimation()
        binding.errorStatus.show()
    }

    private fun retry() {
        binding.errorStatus.hide()
        binding.loadingImageStatus.show()
        lifecycleScope.launch {
            viewModel.getFellowships()
            hideLoadingAnimation()
        }
    }

}