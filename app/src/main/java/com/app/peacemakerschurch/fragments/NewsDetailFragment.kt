package com.app.peacemakerschurch.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.app.peacemakerschurch.MyApplication
import com.app.peacemakerschurch.R
import com.app.peacemakerschurch.databinding.FragmentNewsDetailBinding
import com.app.peacemakerschurch.util.SERMON_URL
import com.app.peacemakerschurch.viewModel.NewsDetailViewModel
import com.squareup.picasso.Picasso

class NewsDetailFragment : Fragment() {

    lateinit var newsDetailViewModel : NewsDetailViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MyApplication).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentNewsDetailBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_news_detail,container,false)
        binding.lifecycleOwner=this

        //Selected News
        val selectedNews = NewsDetailFragmentArgs.fromBundle(requireArguments()).selectedNews
        newsDetailViewModel = NewsDetailViewModel(selectedNews)

        //set up image
        Picasso.get().load(SERMON_URL + newsDetailViewModel.selectedNews.value?.featured_photo).placeholder(R.drawable.white_background).error(
            R.drawable.white_background).into(binding.imageView4)


        //update view
        binding.viewModel = newsDetailViewModel


        return binding.root
    }

}