package com.app.peacemakerschurch.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.app.peacemakerschurch.MyApplication
import com.app.peacemakerschurch.R
import com.app.peacemakerschurch.databinding.FragmentPrayerRequestBinding
import com.app.peacemakerschurch.db.entities.PrayerRequest
import com.app.peacemakerschurch.viewModel.PrayerRequestViewModel
import javax.inject.Inject

class PrayerRequestFragment : Fragment() {

    @Inject
    lateinit var viewModel : PrayerRequestViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MyApplication).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding : FragmentPrayerRequestBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_prayer_request,container,false)

        //validate
        binding.button.setOnClickListener {
            when {
                binding.name.text.toString().trim().isEmpty() -> {
                    binding.name.error = "Required Field!"
                }
                binding.phone.text.toString().trim().isEmpty() -> {
                    binding.phone.error = "Required Field!"
                }
                binding.message.text.toString().trim().isEmpty() -> {
                    binding.message.error = "Required Field!"
                }
                else -> {
                    val data = PrayerRequest(0,binding.name.text.toString(),binding.phone.text.toString(),binding.email.text.toString(),binding.message.text.toString(),"2020-10-19 10:55:37","2020-10-19 10:55:37")
                    viewModel.postPrayerRequest(data.name,data.phone,data.email,data.description)
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        }

        viewModel.prayerRequestResponse.observe(viewLifecycleOwner, Observer {
            if (it != null)
            {
                binding.progressBar.visibility = View.GONE
                Toast.makeText(context,it.response_message,Toast.LENGTH_LONG).show()
                val alertDialogBuilder  = AlertDialog.Builder(context)
                alertDialogBuilder.setMessage(it.response_message)
                alertDialogBuilder.setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                    binding.name.text = null
                    binding.phone.text = null
                    binding.email.text = null
                    binding.message.text = null

                    dialogInterface.dismiss()
                }
                alertDialogBuilder.show()
            }
        })


        return binding.root
    }



}

