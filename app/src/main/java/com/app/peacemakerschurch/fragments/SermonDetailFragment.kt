package com.app.peacemakerschurch.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.app.peacemakerschurch.MyApplication
import com.app.peacemakerschurch.R
import com.app.peacemakerschurch.adapters.SermonAdapter
import com.app.peacemakerschurch.databinding.FragmentSermonDetailBinding
import com.app.peacemakerschurch.db.entities.Sermon
import com.app.peacemakerschurch.util.*
import com.app.peacemakerschurch.viewModel.SermonDetailViewModel
import com.app.peacemakerschurch.viewModel.SermonViewModel
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard
import java.lang.Exception
import javax.inject.Inject

class SermonDetailFragment : Fragment() {

    @Inject
    lateinit var sermonViewModel: SermonViewModel
    lateinit var sermonDetailViewModel: SermonDetailViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MyApplication).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //set up view binding
        val binding: FragmentSermonDetailBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_sermon_detail, container, false)
        binding.lifecycleOwner = this

        //set Up sermon recyclerview
        val adapter = SermonAdapter(SermonAdapter.OnClickListener{
            it.let {
               //Update Current video selected in recyclerview
                getVideo(SERMON_URL + it.url, binding.videoPalyer)
                sermonDetailViewModel.updateSermon(it)
            }
        })

        //bind sermon view
        binding.recyclerview.adapter = adapter

        sermonViewModel.response.observe(viewLifecycleOwner, Observer {
            it?.let {response ->
                when(response) {
                    is Success<*> -> adapter.submitList(response.data as MutableList<Sermon>?)
                    is Error -> handleApiError(response)
                }
            }
        })

//        sermonViewModel.status.observe(viewLifecycleOwner, Observer {
//            it?.let {
//                bindStatus(binding.loadingImageStatus,it)
//            }
//        })


        //get selected sermon
        val sermonProperty = SermonDetailFragmentArgs.fromBundle(requireArguments()).selectedSermon
         sermonDetailViewModel = SermonDetailViewModel(sermonProperty)

        //set up video
        getVideo(SERMON_URL + sermonDetailViewModel.selectedSermon.value?.url, binding.videoPalyer)


        //update view
        binding.viewModel = sermonDetailViewModel

        return binding.root

    }

    override fun onStop() {
        super.onStop()
        JCVideoPlayerStandard.releaseAllVideos()
        JCVideoPlayer.releaseAllVideos()
    }

    private fun displayError(e: Exception) {
        Log.i("Error", e.message.toString())
    }

    //set up video player
    private fun getVideo(url: String?, video: JCVideoPlayerStandard) {
        video.setUp(url, JCVideoPlayerStandard.SCREEN_LAYOUT_LIST)
        video.battery_level.visibility = View.GONE
        video.batteryTimeLayout.visibility = View.GONE
        video.backButton.visibility = View.GONE
        //auto play
        video.startButton.performClick()

    }


}