package com.app.peacemakerschurch.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.app.peacemakerschurch.MyApplication
import com.app.peacemakerschurch.R
import com.app.peacemakerschurch.adapters.SampleVideoAdapter
import com.app.peacemakerschurch.databinding.FragmentSermonBinding
import com.app.peacemakerschurch.db.entities.Sermon
import com.app.peacemakerschurch.util.*
import com.app.peacemakerschurch.viewModel.SermonViewModel
import com.google.android.material.snackbar.Snackbar
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class SermonFragment : Fragment() {
    @Inject
    lateinit var viewModel : SermonViewModel

    private lateinit var adapter: SampleVideoAdapter
    private lateinit var binding: FragmentSermonBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MyApplication).appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
         binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sermon, container, false)

        //observe and set value to adapter
         prepareSermonData()
        //set adapter
         setUpAdapter()
        //observe when item is clicked and navigate
        navigateToClickEvent()

        return binding.root
    }

    private fun prepareSermonData() {
        viewModel.response.observe(viewLifecycleOwner, Observer {
            it?.let {response ->
                when(response) {
                    is Success<*> -> displayData(response.data as List<Sermon>)
                    is Error -> {
                        errorDisplay()
                        handleApiError(response) { retry() }
                    }
                }
            }
        })
    }

    private fun displayData(data: List<Sermon>) {
        hideLoadingAnimation()
        adapter.submitList(data)
    }

    private fun hideLoadingAnimation() {
        binding.loadingImageStatus.hide()
    }

    private fun setUpAdapter() {
        adapter = SampleVideoAdapter(SampleVideoAdapter.OnClickLickListener{
            viewModel.navigateToSelectedSermon(it)
        })
        binding.recyclerview.adapter = adapter

        binding.recyclerview.setOnScrollChangeListener { view, i, i2, i3, i4 ->
            JCVideoPlayerStandard.BACKUP_PLAYING_BUFFERING_STATE
            JCVideoPlayer.BACKUP_PLAYING_BUFFERING_STATE
        }
    }

    private fun navigateToClickEvent() {
        viewModel.navigateToSelectedSermon.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                this.findNavController().navigate(SermonFragmentDirections.actionSermonFragmentToSermonDetailFragment(it))
                viewModel.navigateToSelectedSermonComplete()
            }
        })
    }

    private fun errorDisplay() {
       hideLoadingAnimation()
        binding.errorStatus.show()
    }

    private fun retry() {
        binding.errorStatus.hide()
        binding.loadingImageStatus.show()
        lifecycleScope.launch {
            viewModel.getSermons()
            hideLoadingAnimation()
        }
    }



    override fun onPause() {
        super.onPause()
        JCVideoPlayerStandard.releaseAllVideos()
        JCVideoPlayer.releaseAllVideos()
    }

}