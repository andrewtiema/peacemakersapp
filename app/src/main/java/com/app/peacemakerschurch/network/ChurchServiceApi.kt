package com.app.peacemakerschurch.network

import com.app.peacemakerschurch.db.entities.FellowshipResponse
import com.app.peacemakerschurch.db.entities.NewsResponse
import com.app.peacemakerschurch.db.entities.PrayerRequestResponse
import com.app.peacemakerschurch.db.entities.SermonResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*


interface ChurchServiceApi {

    @GET("sermons")
    fun getSermonsAsync(): Deferred<SermonResponse>

    @GET("fellowships")
     fun getFellowshipsAsync() : Deferred<FellowshipResponse>

    @GET("news")
    fun getNewsAsync(): Deferred<NewsResponse>


    @FormUrlEncoded
    @POST("prayer_request")
    fun postPrayerRequestAsync(
        @Field("name") name : String,
        @Field("email") email :String,
        @Field("phone") phone : String,
        @Field("description") description : String
    ): Deferred<PrayerRequestResponse>


}
