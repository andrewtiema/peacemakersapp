package com.app.peacemakerschurch.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.peacemakerschurch.db.entities.Fellowship
import com.app.peacemakerschurch.network.ChurchServiceApi
import com.app.peacemakerschurch.util.ApiStatus
import com.app.peacemakerschurch.util.Error
import com.app.peacemakerschurch.util.Result
import com.app.peacemakerschurch.util.Success
import retrofit2.HttpException
import javax.inject.Inject

class DiscipleshipRepository @Inject constructor(private val apiService : ChurchServiceApi) {

    private val _response = MutableLiveData<Result>()
    val response : LiveData<Result>
    get() = _response

    private val _status = MutableLiveData<ApiStatus>()
    val status : LiveData<ApiStatus>
    get() = _status

    suspend fun fetchFellowships() : LiveData<Result> {

        try {
            val fellowships = apiService.getFellowshipsAsync()
            val fellowshipData = fellowships.await()
            _response.value = Success(fellowshipData.data)
        }
        catch (throwable : Throwable){

            when(throwable) {
                is HttpException -> {
                    _response.value = Error(false,throwable.code(),throwable.response()?.errorBody())
                }
                else -> {
                    _response.value = Error(true, null, null)
                }
            }
        }

        return response
    }

}