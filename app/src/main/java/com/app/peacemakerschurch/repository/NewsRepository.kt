package com.app.peacemakerschurch.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.peacemakerschurch.network.ChurchServiceApi
import com.app.peacemakerschurch.util.Error
import com.app.peacemakerschurch.util.Result
import com.app.peacemakerschurch.util.Success
import retrofit2.HttpException
import javax.inject.Inject

class NewsRepository @Inject constructor(private val apiService: ChurchServiceApi) {

    private val _response = MutableLiveData<Result>()
    val response : LiveData<Result>
    get() = _response

    suspend fun fetchNews() : LiveData<Result>{
        try {
            val news = apiService.getNewsAsync()
            val newsData = news.await()
            _response.value = Success(newsData.data)

        }catch (throwable : Throwable) {

            when(throwable) {
                is HttpException -> {
                    _response.value = Error(false,throwable.code(),throwable.response()?.errorBody())
                }
                else -> {
                    _response.value = Error(true, null, null)
                }
            }

        }

        return response
    }
}