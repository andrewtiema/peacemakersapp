package com.app.peacemakerschurch.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.peacemakerschurch.db.entities.PrayerRequest
import com.app.peacemakerschurch.db.entities.PrayerRequestResponse
import com.app.peacemakerschurch.network.ChurchServiceApi
import javax.inject.Inject

class PrayerRequestRepository @Inject constructor(private val apiService: ChurchServiceApi) {

    private val _response = MutableLiveData<PrayerRequest>()
    val response : LiveData<PrayerRequest>
    get() = _response

    private val _apiResponse = MutableLiveData<PrayerRequestResponse>()
    val apiResponse : LiveData<PrayerRequestResponse>
    get() = _apiResponse


    suspend fun postPrayerRequestAsync(name: String,email: String,phone:String,description:String)  {
        val request = apiService.postPrayerRequestAsync(name, email, phone, description)

        try {
            val responseData = request.await()

            Log.i("Data", responseData.response_message)

            if (responseData.response_status == 200){
                Log.i("PrayerRequestResponse","request........")
                _apiResponse.value = responseData
               // _response.value = responseData.data
            }else {
                _apiResponse.value = responseData
                Log.i("Response", responseData.response_message)
            }

        }catch (e : Throwable) {
            Log.e("Error",e.message.toString())
            e.message.toString()
        }
    }
}