package com.app.peacemakerschurch.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.peacemakerschurch.db.entities.Sermon
import com.app.peacemakerschurch.network.ChurchServiceApi
import com.app.peacemakerschurch.util.ApiStatus
import com.app.peacemakerschurch.util.Error
import com.app.peacemakerschurch.util.Result
import com.app.peacemakerschurch.util.Success
import retrofit2.HttpException
import java.lang.Exception
import javax.inject.Inject


class SermonRepository @Inject constructor(private val apiService : ChurchServiceApi) {

    private val _status = MutableLiveData<ApiStatus>()
    val status : LiveData<ApiStatus>
    get() = _status

    private val _response = MutableLiveData<Result>()
    val response : LiveData<Result>
        get() = _response

    suspend fun fetchSermons() : LiveData<Result> {

        try {
            val sermons = apiService.getSermonsAsync()
            val sermonData = sermons.await()
            Log.i("SermonRepository","Data Available......")
            _response.value = Success(sermonData.data)

        }catch (throwable: Throwable){

            when(throwable) {
                is HttpException -> {
                    _response.value = Error(false,throwable.code(),throwable.response()?.errorBody())
                }
                else -> {
                    _response.value = Error(true, null, null)
                }
            }
        }

        return response
    }


}
