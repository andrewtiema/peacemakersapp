package com.app.peacemakerschurch.util

enum class ApiStatus {
    LOADING,
    ERROR,
    SUCCESS
}