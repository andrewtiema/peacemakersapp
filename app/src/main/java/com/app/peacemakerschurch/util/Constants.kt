package com.app.peacemakerschurch.util

const val BASE_URL = "http://peacemakers.devstart.co.ke/portal/api/"
const val SERMON_URL = "https://peacemakers.devstart.co.ke/portal/main/storage/app/public/sermons/"
const val FELLOWSHIP_URL = "https://peacemakers.devstart.co.ke/portal/main/storage/app/public/fellowship/"

//const val BASE_URL = "https://mars.udacity.com/"