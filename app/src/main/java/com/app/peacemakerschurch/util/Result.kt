package com.app.peacemakerschurch.util

import okhttp3.ResponseBody

sealed class Result
data class Success<T>(val data : T) : Result()
data class Error(val isNetworkError: Boolean,
                 val errorCode: Int?,
                 val errorBody: ResponseBody?) : Result()
