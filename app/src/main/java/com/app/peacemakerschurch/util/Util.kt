package com.app.peacemakerschurch.util


import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.app.peacemakerschurch.R
import com.google.android.material.snackbar.Snackbar

fun View.snackbar(message: String, action: (() -> Unit)? = null) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_INDEFINITE)
    action?.let {
        snackbar.setAction("Retry") {
            it()
        }
    }
    snackbar.show()
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun Fragment.handleApiError(
    failure: Error,
    retry: (() -> Unit)? = null
) {
    when {
        failure.isNetworkError -> {
            requireView().snackbar("Please check your internet connection", retry)

        }
        else -> {
            val error = failure.errorBody.toString()
            requireView().snackbar(error)
        }
    }
}
