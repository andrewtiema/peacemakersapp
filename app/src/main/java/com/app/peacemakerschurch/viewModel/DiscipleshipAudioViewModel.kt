package com.app.peacemakerschurch.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.peacemakerschurch.db.entities.Fellowship
import javax.inject.Inject

class DiscipleshipAudioViewModel @Inject constructor(fellowship: Fellowship) : ViewModel(){

    private val _selectedDiscipleship = MutableLiveData<Fellowship>()
    val selectedDiscipleship : LiveData<Fellowship>
        get() = _selectedDiscipleship

    init {
        _selectedDiscipleship.value = fellowship
    }
}