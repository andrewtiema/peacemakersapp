package com.app.peacemakerschurch.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.peacemakerschurch.db.entities.Fellowship
import com.app.peacemakerschurch.repository.DiscipleshipRepository
import com.app.peacemakerschurch.util.ApiStatus
import com.app.peacemakerschurch.util.Result
import kotlinx.coroutines.*
import javax.inject.Inject

class DiscipleshipViewModel @Inject constructor(private val discipleship : DiscipleshipRepository) : ViewModel() {

    private var _response = MutableLiveData<Result>()
    val response : LiveData<Result>
    get() = _response

    //set up navigation
    private val _navigateToSelectedDiscipleship = MutableLiveData<Fellowship>()
    val navigateToSelectedDiscipleship : LiveData<Fellowship>
    get() = _navigateToSelectedDiscipleship

    //instantiate coroutine scope
    private val coroutineScope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    init {
        coroutineScope.launch {
            getFellowships()
        }
    }

     suspend fun getFellowships() : LiveData<Result>{
         _response.value = discipleship.fetchFellowships().value
         return response
    }

    fun navigateToSelectedDiscipleship(discipleship : Fellowship) {
        _navigateToSelectedDiscipleship.value = discipleship
    }

    fun navigateToSelectedDiscipleshipDone() {
        _navigateToSelectedDiscipleship.value = null
    }

    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }
}