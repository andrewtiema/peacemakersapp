package com.app.peacemakerschurch.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.peacemakerschurch.db.entities.News
import javax.inject.Inject

class NewsDetailViewModel @Inject constructor(news : News) : ViewModel() {

    private val _selectedNews = MutableLiveData<News>()
    val selectedNews : LiveData<News>
    get() = _selectedNews

    init {
        _selectedNews.value = news
    }

}