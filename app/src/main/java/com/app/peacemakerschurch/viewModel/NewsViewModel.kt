package com.app.peacemakerschurch.viewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.peacemakerschurch.db.entities.News
import com.app.peacemakerschurch.repository.NewsRepository
import com.app.peacemakerschurch.util.Result
import kotlinx.coroutines.*
import javax.inject.Inject

class NewsViewModel @Inject constructor(private val newsRepository: NewsRepository) : ViewModel() {

    private var _response = MutableLiveData<Result>()
    val response : LiveData<Result>
    get() = _response

    private val _navigateToSelectedNews = MutableLiveData<News>()
    val navigateToSelectedNews : LiveData<News>
    get() = _navigateToSelectedNews

    private val coroutineScope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    init {
        viewModelScope.launch {
            getNews()
        }
    }


     suspend fun getNews() : LiveData<Result>{
        _response.value = newsRepository.fetchNews().value
        return response
    }

    fun navigateToSelectedNews(news : News) {
        _navigateToSelectedNews.value = news
    }

    fun navigateToSelectedNewsDone() {
        _navigateToSelectedNews.value = null
    }

    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }
}