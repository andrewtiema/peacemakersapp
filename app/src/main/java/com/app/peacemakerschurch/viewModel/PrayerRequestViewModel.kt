package com.app.peacemakerschurch.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.peacemakerschurch.db.entities.PrayerRequestResponse
import com.app.peacemakerschurch.repository.PrayerRequestRepository
import kotlinx.coroutines.*
import retrofit2.Response
import javax.inject.Inject

class PrayerRequestViewModel @Inject constructor(private val prayerRequestRepository: PrayerRequestRepository) : ViewModel() {

    private val _prayerRequestResponse = MutableLiveData<PrayerRequestResponse>()
    val prayerRequestResponse : LiveData<PrayerRequestResponse>
    get() = _prayerRequestResponse

    //start a thread
    private val coroutineScope = CoroutineScope(Dispatchers.Main + SupervisorJob())


    fun postPrayerRequest (name:String,email:String,phone:String,description: String) {

        coroutineScope.launch {
           prayerRequestRepository.postPrayerRequestAsync(name, email, phone, description)
            _prayerRequestResponse.value = prayerRequestRepository.apiResponse.value
        }
    }

    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }
}