package com.app.peacemakerschurch.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.peacemakerschurch.db.entities.Sermon
import javax.inject.Inject

class SermonDetailViewModel @Inject constructor(sermon: Sermon) : ViewModel() {

    private val _selectedSermon = MutableLiveData<Sermon>()
    val selectedSermon:LiveData<Sermon>
    get() = _selectedSermon

    //set update
    fun updateSermon(updatedVideo : Sermon) {
        _selectedSermon.value = updatedVideo
    }


    init {
        _selectedSermon.value = sermon
    }
}