package com.app.peacemakerschurch.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.peacemakerschurch.db.entities.Sermon
import com.app.peacemakerschurch.repository.SermonRepository
import com.app.peacemakerschurch.util.Result
import kotlinx.coroutines.*
import javax.inject.Inject

class SermonViewModel @Inject constructor(private val sermonRepository: SermonRepository) : ViewModel() {

    //set navigation
    private val _navigateToSelectedSermon = MutableLiveData<Sermon>()
    val navigateToSelectedSermon:LiveData<Sermon>
    get() = _navigateToSelectedSermon

    private val _response = MutableLiveData<Result>()
    val response : LiveData<Result>
    get() = _response

    private val coroutineScope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    init {
        coroutineScope.launch {
            getSermons()
        }
    }

      suspend fun getSermons() : LiveData<Result> {
        _response.value = sermonRepository.fetchSermons().value
         return response
    }


    fun navigateToSelectedSermon(sermon: Sermon) {
        _navigateToSelectedSermon.value = sermon
    }

    fun navigateToSelectedSermonComplete() {
        _navigateToSelectedSermon.value=null
    }

    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
        Log.i("SermonViewModel","ViewModelCleared")
    }

}