package com.app.peacemakerschurch.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.peacemakerschurch.repository.SermonRepository

class SermonViewModelFactory (private val sermonRepository: SermonRepository, private val application: Application): ViewModelProvider.Factory{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SermonViewModel::class.java)){
            return SermonViewModel(sermonRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}